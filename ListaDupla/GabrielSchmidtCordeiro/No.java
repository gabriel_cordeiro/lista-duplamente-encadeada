package GabrielSchmidtCordeiro;

/**
 *
 * @author Gabriel S. Cordeiro <gabrielscordeiro2012@gmail.com>
 * @todo Classe que implementa e manipula os nós da lista
 */
public class No {

    private No anterior;
    private No proximo;
    private String valor;
    private NoCidade cidade;

    public No getAnterior() {
        return anterior;
    }

    public void setAnterior(No anterior) {
        this.anterior = anterior;
    }

    public No getProximo() {
        return proximo;
    }

    public void setProximo(No proximo) {
        this.proximo = proximo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public NoCidade getCidade() {
        return cidade;
    }

    public void setCidade(NoCidade cidade) {
        this.cidade = cidade;
    }

}
