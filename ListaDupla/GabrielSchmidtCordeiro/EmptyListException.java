package GabrielSchmidtCordeiro;


/**
 *
 * @author Gabriel S. Cordeiro <gabrielscordeiro2012@gmail.com>
 */

public class EmptyListException extends NullPointerException {

    public EmptyListException() {
        super();
    }

    public EmptyListException(String msg) {
        super(msg);
    }
}
