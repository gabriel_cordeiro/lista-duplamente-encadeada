package GabrielSchmidtCordeiro;

/**
 * @author Gabriel S. Cordeiro <gabrielscordeiro2012@gmail.com>
 * Classe que implementa e realiza manipulação da lista.
 */
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Lista {

    private No primeiro = null, ultimo = null;
    private NoCidade ultimaCidade = null;

    public void setPrimeiro(No primeiro) {
        this.primeiro = primeiro;
    }

    public No getPrimeiro() {
        return primeiro;
    }

    public void setUltimo(No ultimo) {
        this.ultimo = ultimo;
    }

    public No getUltimo() {
        return ultimo;
    }

    public NoCidade getUltimaCidade() {
        return ultimaCidade;
    }

    public void setUltimaCidade(NoCidade ultimaCidade) {
        this.ultimaCidade = ultimaCidade;
    }


    public ArrayList<String> Listar() throws EmptyListException {
        ArrayList<String> lista = new ArrayList<String>();
        if (primeiro == null) {
            throw new EmptyListException("A lista esta vazia!");
        } else {
            No aux = getPrimeiro();
            while (aux != null) {
                String vl = aux.getValor();
                lista.add(vl);
                aux = aux.getProximo();
            }
            return lista;
        }
    }

    public boolean Procura(String valor) {
        No aux = getPrimeiro();
        while (aux != null) {
            if (valor.equals(aux.getValor())) {
                return true;
            }
            aux = aux.getProximo();
        }
        return false;
    }

    public No procuraNo(String estado) {
        No aux = getPrimeiro();
        while (aux != null) {
            if (estado.equals(aux.getValor())) {
                return aux;
            }
            aux = aux.getProximo();
        }
        return null;
    }

    public void Insere_Inicio(String valor) throws ExistentValueException {
        boolean procura = false;
        procura = Procura(valor);
        if (procura == false) {
            No novo = new No();
            if (primeiro == null) {
                novo.setValor(valor);
                setPrimeiro(novo);
                setUltimo(novo);
            } else {
                primeiro.setAnterior(novo);
                novo.setValor(valor);
                novo.setProximo(primeiro);
                setPrimeiro(novo);
            }
        } else {
            throw new ExistentValueException("Valor já existe na lista!");
        }
    }

    public void Insere_Fim(String valor) throws ExistentValueException {
        No novo = new No();
        boolean procura = false;
        procura = Procura(valor);
        if (procura == true) {
            throw new ExistentValueException("Valor já existe na lista!");
        } else if (ultimo == null) {
            novo.setValor(valor);
            primeiro = novo;
            ultimo = novo;
        } else {
            ultimo.setProximo(novo);
            novo.setValor(valor);
            ultimo = novo;
        }
    }

    public void Insere_Cidade_Estado(String nomeEstado, String nomeCidade) throws ExistentValueException {
        if (procuraNo(nomeEstado) != null) {
            No listaAux = procuraNo(nomeEstado);

            NoCidade cidade = new NoCidade();
            cidade.setValorCidade(nomeCidade);
            ultimaCidade = cidade;

            if (listaAux.getCidade() == null) {
                cidade.setProximo(null);
                listaAux.setCidade(cidade);
            }else{
                ultimaCidade.setProximo(cidade);
                ultimaCidade = cidade;
            }
            
            JOptionPane.showMessageDialog(null, "Cidade inserida");
        }
    }
}
