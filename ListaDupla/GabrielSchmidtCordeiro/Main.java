package GabrielSchmidtCordeiro;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * @author Gabriel S. Cordeiro <gabrielscordeiro2012@gmail.com>
 */
public class Main {

    public static void main(String[] args) throws ExistentValueException {
        Lista lista = new Lista();
        String valor = null;

        int resp = Integer.parseInt(JOptionPane.showInputDialog("Escolha a opção\n1 - Inserir no Início:\n2 - Inserir no fim:\n3 - Pesquisar um estado:\n4 - Listar estados:\n5 - Inserir cidade:\n9 - Sair:"));
        while (resp != 9) {
            if (resp == 1) {
                valor = JOptionPane.showInputDialog("Insira o nome do estado:");

                try {
                    lista.Insere_Inicio(valor);
                } catch (ExistentValueException e) {
                    e.printStackTrace();
                }
            } else if (resp == 2) {
                valor = JOptionPane.showInputDialog("Insira o nome do estado:");

                try {
                    lista.Insere_Fim(valor);
                } catch (ExistentValueException e) {
                    e.printStackTrace();
                }
            } else if (resp == 3) {
                valor = JOptionPane.showInputDialog("Insira o nome do estado:");

                if (lista.Procura(valor) == true) {
                    JOptionPane.showMessageDialog(null, "Estado encontrado");
                } else {
                    JOptionPane.showMessageDialog(null, "Estado não encontrado");
                }
            } else if (resp == 4) {
                ArrayList<String> listar = new ArrayList<String>();

                try {
                    listar = lista.Listar();
                } catch (EmptyListException e) {
                    e.printStackTrace();
                }
                String valorEstado = "";
                for (String elemento : listar) {
                    valorEstado += elemento + "\n ";
                }

                JOptionPane.showMessageDialog(null, valorEstado);
            } else if (resp == 5) {
                ArrayList<String> listar = new ArrayList<String>();

                try {
                    listar = lista.Listar();
                } catch (EmptyListException e) {
                    e.printStackTrace();
                }
                String valorEstado = "";
                int i = 0;
                for (String elemento : listar) {
                    valorEstado += i + " - " + elemento + "\n ";
                    i++;
                }
                if (lista.Listar().size() > 0) {
                    int valEstado = Integer.parseInt(JOptionPane.showInputDialog("Insira o número correspondente ao estado desejado:\n\n" + valorEstado));
                    String nomeEstado = lista.Listar().get(valEstado);
                    
                    if (lista.Procura(valor) == true) {
                        String nomeCidade = JOptionPane.showInputDialog(null, "Insira o nome da cidade - "+ nomeEstado);
                        lista.Insere_Cidade_Estado(nomeEstado, nomeCidade);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Você deve primeiro cadastrar um estado");
                }
            }
            resp = Integer.parseInt(JOptionPane.showInputDialog("Escolha a opção\n1 - Inserir no Início:\n2 - Inserir no fim:\n3 - Pesquisar um estado:\n4 - Listar estados:\n5 - Inserir cidade:\n9 - Sair:"));
        }
    }

}
