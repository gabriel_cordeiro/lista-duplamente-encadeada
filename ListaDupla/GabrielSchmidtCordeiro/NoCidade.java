package GabrielSchmidtCordeiro;

/**
 *
 * @author Gabriel Schmidt Cordeiro <gabrielscordeiro2012@gmail.com>
 */
public class NoCidade {

    private NoCidade proximo;
    private String valorCidade;

    public NoCidade getProximo() {
        return proximo;
    }

    public void setProximo(NoCidade proximo) {
        this.proximo = proximo;
    }

    public String getValorCidade() {
        return valorCidade;
    }

    public void setValorCidade(String valorCidade) {
        this.valorCidade = valorCidade;
    }

}
